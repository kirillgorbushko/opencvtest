//
//  CameraPreviewCollectionViewCell.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class CameraPreviewCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet fileprivate  weak var previewView: UIView!
    fileprivate var captureSession:CaptureSession?
    @IBOutlet fileprivate weak var photoCameraIMageView: UIImageView!
    
    // MARK: - LifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        startSession()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        captureSession?.relayoutPreview()        
    }
    
    // MARK: - Public
    
    func stopSession() {
        captureSession?.stop()
    }
    
    func startSession() {
        if CaptureSession.hasBackCamera() {
            CaptureSession.checkPermissionForCamera { (granted) in
                if granted {
                    self.captureSession = CaptureSession(self.previewView)
                    self.captureSession?.start()
                }
            }
        }
    }
}
