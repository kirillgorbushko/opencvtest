//
//  MSERViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 30.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class TMViewController : UIViewController {
    
    @IBOutlet fileprivate weak var cameraPreviewImageView: UIImageView!
    @IBOutlet fileprivate weak var templatePreviewImageView: UIImageView!
    @IBOutlet fileprivate weak var analyzePreviewImageView: UIImageView!
    
    fileprivate var wrapper:OpenCVWrapper = OpenCVWrapper()

    fileprivate var currentFrame:UIImage?

    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareUI()
        initTemlateMatch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupMatcher()
    }

    // MARK: - Private
    
    fileprivate func prepareUI() {
        self.analyzePreviewImageView.layer.borderWidth = 1
        self.analyzePreviewImageView.layer.cornerRadius = 2
        self.analyzePreviewImageView.layer.masksToBounds = true
        self.analyzePreviewImageView.layer.borderColor = UIColor.black.cgColor
        
        self.templatePreviewImageView.layer.borderWidth = 1
        self.templatePreviewImageView.layer.cornerRadius = 2
        self.templatePreviewImageView.layer.masksToBounds = true
        self.templatePreviewImageView.layer.borderColor = UIColor.black.cgColor
    }
    
    fileprivate func initTemlateMatch() {
        if let templateImage = //UIImage(named: "square") {
//                                UIImage(named: "sigma") {
                                UIImage(named: "s2") {
            self.wrapper.prepareMatcher(withTemplate: templateImage)
        }
    } 
    
    fileprivate func setupMatcher() {
        let queue = DispatchQueue(label: "analyzeQueue.com")
        
        var stepper:Int = 0
        self.wrapper.startCameraSession(with: self.cameraPreviewImageView) { (image) in
            DispatchQueue.main.async {
                self.currentFrame = image
            }
            
            queue.async {
                stepper += 1
                if stepper % 15 == 0 {
                    stepper = 0
                    self.wrapper.analyze(image)
                } 
            }
        }
        
        self.wrapper.didReceiveAnalyzedImage = { image in 
            DispatchQueue.main.async {
                self.analyzePreviewImageView.image = image
            }
        }
        
        self.wrapper.didReceiveTemplateImage = { image in
            DispatchQueue.main.async {
                self.templatePreviewImageView.image = image
            }
        }
        
        self.wrapper.didDetectObj = { detect in
            
            var color = UIColor.red
            if detect {
                color = UIColor.green
            }
            
            DispatchQueue.main.async {
                self.analyzePreviewImageView.layer.borderColor = color.cgColor
                self.templatePreviewImageView.layer.borderColor = color.cgColor
            }
        }
    }
}
