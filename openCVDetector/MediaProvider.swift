//
//  MediaProvider.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 29.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit
import Photos

final class MediaProvider {

    fileprivate weak var collectionView:UICollectionView?
    fileprivate var previousPreheatRect:CGRect = CGRect.zero
    fileprivate var assetsFetchResult:PHFetchResult<PHAsset>?
    fileprivate var imageManager:PHCachingImageManager?
    
    fileprivate static var assetGridThumbnailSize:CGSize = CGSize.zero

    
    // MARK: - LifeCycle
    
    init(collectionView:UICollectionView?) {
        resetCachedAssets()
        requestPhotos()
        
        self.collectionView = collectionView
        
        if let collectionView = collectionView {
            MediaProvider.assetGridThumbnailSize = CGSize(width: collectionView.frame.height / 5, height: collectionView.frame.width / 3) 
        }
    }
    
    // MARK: - Public
    
    func fetchPhotosFor(selectedIndexPaths:[IndexPath], offset:Int = 0, completion:(([UIImage])->())?) {
        let imageIndexes:[Int] = selectedIndexPaths.map({
            $0.row - offset            
        })
        
        var images:[UIImage?] = []
        let group = DispatchGroup()
        
        for key in imageIndexes {
            group.enter()
            imageAtIndexPath(index: key, handler: { (image) in
                images.append(image)
                group.leave()
            })
            
        }
        group.notify(queue: DispatchQueue.main, execute: {
            let filteredImages:[UIImage] = images.flatMap({$0})
            completion?(filteredImages)
        }) 
    }
    
    func imageAtIndexPath(index:Int, handler:((UIImage?)->())?) {
        thumbnailForIndexPath(index: index, targetSize: PHImageManagerMaximumSize, handler: handler)
    }
    
    func thumbnailForIndexPath(index:Int, targetSize:CGSize, handler:((UIImage?)->())?) {
        if let asset:PHAsset = assetsFetchResult?.object(at: index) {
            imageManager?.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: nil, resultHandler: { (image, info) in
                handler?(image)
            })
        }
    }
    
    func calculateTargetSizeForItemAt(index:Int) -> CGSize {
        if let collectionView = collectionView {
            let heightOfImage:CGFloat = collectionView.frame.size.height / 5
            if let asset:PHAsset = assetsFetchResult?.object(at: index) {
                let imageSize:CGSize = CGSize(width:CGFloat(asset.pixelWidth), height:CGFloat(asset.pixelHeight))
                let koef: CGFloat = imageSize.height / heightOfImage;                
                return CGSize(width:imageSize.width / koef, height: heightOfImage);
            }
        }
        return CGSize.zero
    }
    
    func itemsCount() -> Int {
        if let result = assetsFetchResult {
            return result.count
        }
        return 0
    }
    
    func reload() {
        prepareCameraRollImages()
    }
    
    // MARK: - Cache
    
    func updatePrecheatedRect(controller:UIViewController) {
        if let collectionView = collectionView {
            var loaded = false
            if controller.isViewLoaded && controller.view.window != nil {
                loaded = true
            }
            if (!loaded) {
                return
            }
            
            var preheatRect:CGRect = collectionView.bounds
            preheatRect = preheatRect.insetBy(dx:0, dy: -0.5 * preheatRect.height)
            
            let delta = abs(preheatRect.height / 2 - previousPreheatRect.height / 2)
            if delta > collectionView.bounds.height / 3 {
                var addedIndexPath:[IndexPath] = []
                var removedIndexPath:[IndexPath] = []
                
                computeDifferenceBetweenRect(oldRect: previousPreheatRect, newRect: preheatRect, removedHandler: { (rect) in
                    let indexes = self.indexPathsForElementsInRect(rect: rect)
                    removedIndexPath.append(contentsOf: indexes)
                }, addedHandler: { (rect) in
                    let indexes = self.indexPathsForElementsInRect(rect: rect)
                    addedIndexPath.append(contentsOf: indexes)
                })
                
                let assetsToStartCaching = assetsAtIndexPaths(source: addedIndexPath)
                let assetsToStopCaching = assetsAtIndexPaths(source: removedIndexPath)
                
                imageManager?.startCachingImages(for: assetsToStartCaching, targetSize: MediaProvider.assetGridThumbnailSize, contentMode: .aspectFit, options: nil)
                imageManager?.stopCachingImages(for: assetsToStopCaching, targetSize: MediaProvider.assetGridThumbnailSize, contentMode: .aspectFit, options: nil)
                
                previousPreheatRect = preheatRect
            }
        }
    }
    
    fileprivate func indexPathsForElementsInRect(rect:CGRect) -> [IndexPath] {
        
        let layoutAttributes = collectionView?.collectionViewLayout.layoutAttributesForElements(in: rect)
        if let layoutAttributes = layoutAttributes {
            if layoutAttributes.count > 0 {
                var indexPath:[IndexPath] = []
                for attribute in layoutAttributes {
                    let index:IndexPath = attribute.indexPath
                    indexPath.append(index)
                }
                return indexPath
            }
        }
        return []
    }
    
    fileprivate func assetsAtIndexPaths(source:[IndexPath]) -> [PHAsset] {
        
        var items:[PHAsset] = []
        for index in source {
            if let asset = assetsFetchResult?.object(at: index.row) {
                items.append(asset)
            } 
        }
        
        return items
    }
    
    fileprivate func computeDifferenceBetweenRect(oldRect:CGRect, newRect:CGRect, removedHandler:((CGRect)->()), addedHandler:((CGRect)->())){
        if newRect.intersects(oldRect) {
            let oldMaxY:CGFloat = oldRect.height
            let oldMinY:CGFloat = oldRect.origin.y 
            let newMaxY:CGFloat = newRect.height
            let newMinY:CGFloat = newRect.origin.y 
            
            if (newMaxY > oldMaxY) {
                let rectToAdd:CGRect = CGRect(x:newRect.origin.x, y:oldMaxY, width:newRect.size.width, height:(newMaxY - oldMaxY))
                addedHandler(rectToAdd)
            }
            
            if (oldMinY > newMinY) {
                let rectToAdd:CGRect = CGRect(x:newRect.origin.x, y:newMinY, width:newRect.size.width, height: (oldMinY - newMinY))
                addedHandler(rectToAdd)
            }
            
            if (newMaxY < oldMaxY) {
                let rectToRemove:CGRect = CGRect(x:newRect.origin.x, y:newMaxY, width:newRect.size.width, height:(oldMaxY - newMaxY))
                removedHandler(rectToRemove)
            }
            
            if (oldMinY < newMinY) {
                let rectToRemove:CGRect = CGRect(x:newRect.origin.x, y:oldMinY, width: newRect.size.width, height: (newMinY - oldMinY))
                removedHandler(rectToRemove)
            } 
        } else {
            addedHandler(newRect)
            removedHandler(oldRect)
        }
    }

    // MARK: - Private
            
    fileprivate func resetCachedAssets() {
        imageManager?.stopCachingImagesForAllAssets()
        previousPreheatRect = CGRect.zero
    }
    
    fileprivate func requestPhotos() {
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            prepareCameraRollImages()
        } else {
            requestForAuthorization()
        }
    }
    
    fileprivate func prepareCameraRollImages() {
        imageManager = PHCachingImageManager()
        let allPhotosOption = PHFetchOptions()
        allPhotosOption.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        assetsFetchResult = PHAsset.fetchAssets(with: .image, options: allPhotosOption)
        collectionView?.reloadData()
    }
    
    fileprivate func requestForAuthorization() {
        PHPhotoLibrary.requestAuthorization { (status) in
            if status == .authorized {
                self.prepareCameraRollImages()
                self.collectionView?.reloadData()
            } 
        }
    }
}
