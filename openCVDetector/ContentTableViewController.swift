//
//  ViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 26.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit
import Photos

class ContentTableViewController: UITableViewController {

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }


    // MARK: - Private
    
    fileprivate func setupTableView() {
        tableView.tableFooterView = UIView()
    }
}

extension ContentTableViewController {
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
