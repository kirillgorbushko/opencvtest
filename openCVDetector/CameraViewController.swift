//
//  CameraViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 29.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit
import Photos

final class CameraViewController: UIViewController {
    
    @IBOutlet fileprivate weak var cameraPreviewImageView: UIImageView!
    @IBOutlet fileprivate weak var photoPreviewImageView: UIImageView!
    @IBOutlet fileprivate weak var fakeCollection: UIView!
    
    fileprivate var currentFrame:UIImage?
    fileprivate var assetCollectionPlaceholder: PHObjectPlaceholder?
    fileprivate var assetCollection: PHAssetCollection?

    
    fileprivate var wrapper:OpenCVWrapper = OpenCVWrapper()
    
    // MARK: - LifeCycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            self.wrapper.startCameraSession(with: self.cameraPreviewImageView) { (image) in
                self.currentFrame = image
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction fileprivate func takePhotoAction(_ sender: Any) {
        
        if let image = self.currentFrame {
            photoPreviewImageView.layer.opacity = 1
            photoPreviewImageView.image = image
            
            PHPhotoLibrary.shared().savePhoto(image: image, albumName: "openCVTest", completion: { asset in 
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: { 
                    self.animateImageSave()
                })
            })
        }
        
    }    
    
    fileprivate func animateImageSave() {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock { 
            self.photoPreviewImageView.image = nil
            self.photoPreviewImageView.layer.position = self.photoPreviewImageView.center
        }   
        
        let fadeAnimation = Animator.fadeAnimation(1, toValue: 0)
        
        let boundsAnimation = CABasicAnimation(keyPath: "bounds")
        boundsAnimation.fromValue = photoPreviewImageView.frame
        boundsAnimation.toValue = fakeCollection.frame
        
        let group = CAAnimationGroup()
        group.animations = [fadeAnimation, boundsAnimation]
        group.duration = 0.3
        group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        photoPreviewImageView.layer.add(group, forKey: nil)
        photoPreviewImageView.layer.opacity = 0
        photoPreviewImageView.layer.position = fakeCollection.layer.position
        
        
        CATransaction.commit()
    }
}
