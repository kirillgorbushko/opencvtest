//
//  FaceDetectionViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 11.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class FaceDetectionViewController : UIViewController {
    
    fileprivate var captureSession:openCVCaptureSession?
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession = openCVCaptureSession(cameraView: self.view, scale: UIScreen.main.scale)
        captureSession?.startCapture()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        captureSession?.stopCapture()
    }

}
