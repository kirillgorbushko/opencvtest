//
//  StitchViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class StitchViewController : UIViewController {
        
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var previewImageView: UIImageView!
    
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var imageViewWidthConstraint: NSLayoutConstraint!
    
    fileprivate var dataSource:[UIImage] = []
    fileprivate var wrapper:OpenCVWrapper = OpenCVWrapper()
    fileprivate var resultedImage:UIImage? {
        didSet {
            if let image = self.resultedImage {
                DispatchQueue.main.async {
                    self.previewImageView.image = image
                    self.scrollView.setZoomScale(2, animated: true)
                }
            }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    fileprivate struct Constants {
        static let demoImages:[String] = [
            "pano_19_16_mid",
            "pano_19_20_mid",
            "pano_19_22_mid",
            "pano_19_25_mid"
        ]
    }
        
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localizeUI()
    }
    
    // MARK: - Ibactions
    
    @IBAction func demoButtonAction(_ sender: Any) {
        resultedImage = nil
        dataSource = Constants.demoImages.map({UIImage(named:$0)}).flatMap({$0})
        let set = IndexSet(integer: 0)
        collectionView.reloadSections(set)
    }
    
    @IBAction func selectPhotoAction(_ sender: Any) {

    }
    
    @IBAction func cleanUpAction(_ sender: Any) {
        resultedImage = nil
        previewImageView.image = nil
        
        dataSource = []
        let set = IndexSet(integer: 0)
        collectionView.reloadSections(set)
    }
    
    @objc fileprivate func didTapScrollView() {
        scrollView.setZoomScale(1, animated: true)
    }
    
    @IBAction func stitchAction(_ sender: Any) {
        if dataSource.count > 0 {
            activityIndicator.startAnimating()        
            DispatchQueue.main.async {
                self.resultedImage = self.wrapper.stitch(self.dataSource)
                if self.resultedImage == nil {
                    self.previewImageView.image = UIImage(named: "invalidaData")
                    self.scrollView.setZoomScale(0.5, animated: true)
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectPhotoSegueIdentifier" {
            if let target = segue.destination as? SelectPhotoViewController {
            
                target.didCompletePhotoSelection = { images in 
                    if images.count > 0 {
                        DispatchQueue.main.async {
                            self.dataSource.append(contentsOf: images)
                            let set = IndexSet(integer: 0)
                            self.collectionView.reloadSections(set)
                        }
                    }
                }
                
            }
        }
    }
    
    // MARK: - Private 
    
    fileprivate func localizeUI() {
        title = "Stitch sample"
    } 
    
    fileprivate func setupUI() {
        imageViewHeightConstraint.constant = collectionView.frame.size.height
        imageViewWidthConstraint.constant = collectionView.frame.size.width
        view.layoutIfNeeded()
        
        scrollView.maximumZoomScale = 4.0
        scrollView.minimumZoomScale = 0.5
        scrollView.isScrollEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(StitchViewController.didTapScrollView))
        tapGesture.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(tapGesture)
    }
}

extension StitchViewController : UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension StitchViewController : UICollectionViewDataSource {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:PreviewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: PreviewCollectionViewCell.identifier(), for: indexPath) as! PreviewCollectionViewCell
        let image = dataSource[indexPath.row]
        cell.previewImageView.image = image
        cell.photoNameLabel.text = "Photo \(indexPath.row + 1)" 
        return cell
    }
}

extension StitchViewController : UIScrollViewDelegate {
    // MARK: - UIScrollViewDelegate
    func viewForZooming(in scrollView:UIScrollView) -> UIView? {
        return self.previewImageView.superview
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView){
        let offsetX = max((scrollView.bounds.width - scrollView.contentSize.width) * 0.5, 0)
        let offsetY = max((scrollView.bounds.height - scrollView.contentSize.height) * 0.5, 0)
        scrollView.contentInset = UIEdgeInsetsMake(offsetY, offsetX, 0, 0)        
    }
}
