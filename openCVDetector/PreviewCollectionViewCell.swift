//
//  PreviewCollectionViewCell.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit

enum PreviewType {
    case flatPhoto
    case selectablePhoto
}

final class PreviewCollectionViewCell: UICollectionViewCell {

    var previewType:PreviewType = .flatPhoto {
        didSet {
            if previewType == .selectablePhoto {
                overlayView.backgroundColor = UIColor.clear                
                verificationImageView.layer.cornerRadius = verificationImageView.frame.height / 2
                verificationImageView.layer.masksToBounds = true
                verificationImageView.backgroundColor = UIColor.white
            }
        }
    }
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var photoNameLabel: UILabel!
    
    @IBOutlet fileprivate weak var overlayView: UIView!
    @IBOutlet fileprivate weak var verificationImageView: UIImageView!
    
    // MARK: - LifeCycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        previewImageView.image = nil
        photoNameLabel.text = ""

        if previewType == .selectablePhoto {
            
            verificationImageView.alpha = 0
            overlayView.backgroundColor = UIColor.clear
            verificationImageView.tintColor = UIColor.clear
            
            setItemSelected(selected: false)
        }
    }
    
    func setItemSelected(selected:Bool) {
        if previewType == .selectablePhoto {
            if selected {            
                UIView.animate(withDuration: 0.3, animations: { 
                    self.overlayView.backgroundColor = UIColor.white.withAlphaComponent(0.25)
                    self.verificationImageView.tintColor = UIColor.init(colorLiteralRed: 25/255, green: 130/255, blue: 222/255, alpha: 1)
                    self.verificationImageView.alpha = 1
                })
            } else {
                UIView.animate(withDuration: 0.3, animations: { 
                    self.overlayView.backgroundColor = UIColor.clear
                    self.verificationImageView.tintColor = UIColor.clear
                    self.verificationImageView.alpha = 0
                })
            }        
        }
    }
    
}
