//
//  TemplateMatch.h
//  openCVDetector
//
//  Created by Kirill Gorbushko on 06.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TemplateMatchDelegate <NSObject>

@optional
- (void)templateMatchDidReceiveTemplateImage:(UIImage * _Nonnull)templateImage;
- (void)templateMatchDidReceiveAnalyzedImage:(UIImage * _Nonnull)analyzedImage;
- (void)templateMatchDidDetectObj:(BOOL)detect;

@end

@interface TemplateMatch : NSObject

@property (weak, nonatomic, nullable) id <TemplateMatchDelegate> delegate;

- (nonnull instancetype)initWithtemplateImage:( UIImage * _Nonnull )templateImage;
- (void)analyzeImage:(UIImage * _Nonnull)source;

@end
