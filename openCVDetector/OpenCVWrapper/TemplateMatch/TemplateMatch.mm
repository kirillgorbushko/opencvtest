//
//  TemplateMatch.m
//  openCVDetector
//
//  Created by Kirill Gorbushko on 06.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

#import "TemplateMatch.h"
#include <opencv2/opencv.hpp>

#import "UIImage+openCV.h"

using namespace cv;

@interface TemplateMatch()

@property (strong, nonatomic) UIImage *templateImage;
@property (assign, nonatomic) cv::Mat templateImageMat; 

@end

@implementation TemplateMatch {
    int match_method;
}

#pragma mark - Lifecycle

- (nonnull instancetype)initWithtemplateImage:( UIImage * _Nonnull )templateImage
{
    self = [super init];
    if (self) {
        self.templateImage = templateImage;
        self.templateImageMat = [templateImage CVMat];
        match_method = CV_TM_SQDIFF;
    }
    return self;
}

#pragma mark - Public

- (void)analyzeImage:(UIImage * _Nonnull)source 
{
    cv::Mat sourceImageMat = [source CVMat];
    cv::Mat processed = [self matching:sourceImageMat];
    
    sourceImageMat.release();
    processed.release();
}

#pragma mark - Private

- (cv::Mat)matching:(cv::Mat &)analyzedImage
{    
    cv::Mat input = analyzedImage;
    
    cv::Mat gray;
    cv::cvtColor(input,gray,CV_BGR2GRAY);
    
    cv::Mat templ;
    _templateImageMat.copyTo(templ);
    
    cv::Mat img = input;
    cv::Mat result;
        /// Create the result matrix
    int result_cols =  img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;
    
    result.create( result_cols, result_rows, CV_32FC1 );
        
        /// Do the Matching and Normalize
    matchTemplate( img, templ, result, match_method  );
    normalize( result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
    
        /// Localizing the best match with minMaxLoc
    double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
    cv::Point matchLoc;
    
    minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );
    
        /// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
    if( match_method  == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED )
        { matchLoc = minLoc; }
    else
        { matchLoc = maxLoc; }
    
        /// Show me what you got
    cv::rectangle( input, matchLoc, cv::Point( matchLoc.x + templ.cols , matchLoc.y + templ.rows ), cv::Scalar::all(0), 2, 8, 0 );
    cv::rectangle( result, matchLoc, cv::Point( matchLoc.x + templ.cols , matchLoc.y + templ.rows ), cv::Scalar::all(0), 2, 8, 0 );
    
    BOOL detect = (matchLoc.x && matchLoc.y);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(templateMatchDidReceiveAnalyzedImage:)]) {
        UIImage *resultImg = detect ? [UIImage imageWithCVMat:input] : [UIImage new];
        [self.delegate templateMatchDidReceiveAnalyzedImage:resultImg];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(templateMatchDidReceiveTemplateImage:)]) {
        UIImage *resultImg = detect ? [UIImage imageWithCVMat:result] : [UIImage new];
        [self.delegate templateMatchDidReceiveTemplateImage:resultImg];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(templateMatchDidDetectObj:)]) {
        [self.delegate templateMatchDidDetectObj:detect];
    }
    
    return result;
}

@end
