//
//  stitching.h
//  CVOpenTemplate
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

#ifndef CVOpenTemplate_Header_h
#define CVOpenTemplate_Header_h
#include <opencv2/opencv.hpp>

cv::Mat stitch (std::vector <cv::Mat> & images);

#endif
