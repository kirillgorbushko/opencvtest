#import <Foundation/Foundation.h>
#include <opencv2/opencv.hpp>

@interface FPS : NSObject

+ (void)draw:(cv::Mat)rgb;

@end
