//
//  OpenCVWrapper.h
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

//match
- (void)prepareMatcherWithTemplate:(UIImage * _Nonnull)source;
- (void)analyzeImage:(UIImage * _Nonnull)source;

@property (copy, nonatomic, nullable) void (^didReceiveTemplateImage)(UIImage * _Nonnull);
@property (copy, nonatomic, nullable) void (^didReceiveAnalyzedImage)(UIImage * _Nonnull);
@property (copy, nonatomic, nullable) void (^didDetectObj)(BOOL);

//stitch
- (UIImage * _Nonnull)stitchImages:( NSArray <UIImage *> * _Nonnull)sourceImages;

//camera
- (void)startCameraSessionWith:(UIImageView * _Nonnull)parentView captureBlock:(void (^ _Nullable)(UIImage * _Nonnull))captureImageBlock;

@end
