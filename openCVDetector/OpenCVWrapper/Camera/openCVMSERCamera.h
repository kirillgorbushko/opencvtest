//
//  openCVMSERCamera.h
//  openCVDetector
//
//  Created by Kirill Gorbushko on 11.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface openCVMSERCamera : NSObject

- (void)setupCamera:(UIImageView * _Nonnull)parentView;
- (void)startCamera;

@end
