//
//  openCVCaptureSession.m
//  VisualHeartRate
//
//  Created by Kirill Gorbushko on 29.02.16.
//  Copyright © 2016 - present thinkmobiles. All rights reserved.
//

#import "openCVCaptureSession.h"
#import "UIImage+openCV.h"
#import "VideoCamera.h"

@interface openCVCaptureSession ()

@property (strong, nonatomic) VideoCamera *videoCamera;

@property (assign, nonatomic) cv::CascadeClassifier faceDetector;
@property (assign, nonatomic) cv::CascadeClassifier mouthDetector;

@property (assign, nonatomic) std::vector <cv::Rect> faceRects;
@property (assign, nonatomic) std::vector <cv::Mat> faceImgs;

@property (assign, nonatomic) CGFloat scale;

@end

@implementation openCVCaptureSession

#pragma mark - Public

- (instancetype)initWithCameraView:(UIView *)view scale:(CGFloat)scale
{
    self = [super init];
    if (self) {
        
        [self prepareOpenCVVideoSessionOnView:view];
        if (scale) {
            self.scale = scale;
        }
//        [self prepareHaarcascade];
        _faceDetector = [self classifierForFileName:@"haarcascade_frontalface_alt"];
        _mouthDetector = [self classifierForFileName:@"haarcascade_mcs_mouth"];
    }
    
    return self;
}

- (void)startCapture
{
    [self.videoCamera start];
}

- (void)stopCapture
{
    [self.videoCamera stop];
}

- (NSArray *)detectedFaces
{
    NSMutableArray *facesArray = [NSMutableArray array];
    for( std::vector<cv::Rect>::const_iterator r = _faceRects.begin(); r != _faceRects.end(); r++ ) {
        CGRect faceRect = CGRectMake(_scale*r->x/480., _scale*r->y/640., _scale*r->width/480., _scale*r->height/640.);
        [facesArray addObject:[NSValue valueWithCGRect:faceRect]];
    }
    return facesArray;
}

- (UIImage *)faceWithIndex:(NSInteger)idx
{
    cv::Mat img = self->_faceImgs[idx];
    UIImage *ret = [UIImage imageWithCVMat:img];
    return ret;
}

#pragma mark - CvVideoCameraDelegate

- (void)processImage:(cv::Mat&)image
{
//    [self detectAndDrawFacesOn:image scale:_scale];
    [self detectAndDisplay:image];
}

#pragma mark - Private

- (void)prepareOpenCVVideoSessionOnView:(UIView *)view
{
    self.videoCamera = [[VideoCamera alloc] initWithParentView:view];
    self.videoCamera.delegate = self;
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetiFrame960x540;//AVCaptureSessionPresetHigh;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.rotateVideo = NO;
}

- (void)prepareHaarcascade
{
    NSString *option = @"Mouth";
    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:option ofType:@"xml"];
    const CFIndex CASCADE_NAME_LEN = 2048;
    char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
    CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
    _faceDetector.load(CASCADE_NAME);
    free(CASCADE_NAME);
}

- (cv::CascadeClassifier)classifierForFileName:(NSString *)fileName
{
    cv::CascadeClassifier detector;
    NSString *option = fileName;
    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:option ofType:@"xml"];
    const CFIndex CASCADE_NAME_LEN = 2048;
    char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
    CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
    detector.load(CASCADE_NAME);
    free(CASCADE_NAME);
    return detector;
}

- (void)detectAndDrawFacesOn:(cv::Mat&) img scale:(double)scale
{
    
    int i = 0;
    double t = 0;
    
    const static cv::Scalar colors[] =  { CV_RGB(0,0,255),
        CV_RGB(0,128,255),
        CV_RGB(0,255,255),
        CV_RGB(0,255,0),
        CV_RGB(255,128,0),
        CV_RGB(255,255,0),
        CV_RGB(255,0,0),
        CV_RGB(255,0,255)} ;
    cv::Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
    
    cvtColor( img, gray, cv::COLOR_BGR2GRAY );
    resize( gray, smallImg, smallImg.size(), 0, 0, cv::INTER_LINEAR);
    equalizeHist( smallImg, smallImg );
    
    t = (double)cvGetTickCount();
    double scalingFactor = 1.1;
    int minRects = 3;
    cv::Size minSize(30,30);
    
    _faceDetector.detectMultiScale( smallImg, self->_faceRects, scalingFactor, minRects, 0 | cv::CASCADE_SCALE_IMAGE, minSize );
    
//    t = (double)cvGetTickCount() - t;
//    printf( "detection time = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );
    std::vector<cv::Mat> faceImages;
    
    for( std::vector<cv::Rect>::const_iterator r = _faceRects.begin(); r != _faceRects.end(); r++, i++ ) {
        cv::Mat smallImgROI;
        cv::Point center;
        cv::Scalar color = colors[i%8];
        std::vector<cv::Rect> nestedObjects;
        rectangle(img,
                  cvPoint(cvRound(r->x*scale), cvRound(r->y*scale)),
                  cvPoint(cvRound((r->x + r->width-1)*scale), cvRound((r->y + r->height-1)*scale)),
                  color, 1, 8, 0);
        smallImgROI = smallImg(*r);
        
        faceImages.push_back(smallImgROI.clone());
        
        if (r->height > 115) {
            NSLog(@"Open, %i, %i : %i, %i", r->height, r->width, r->x, r->y) ;
        } else {
            NSLog(@"Close,  %i, %i : %i, %i", r->height, r->width, r->x, r->y);
        }
    }
    
    @synchronized(self) {
        _faceImgs = faceImages;
    }
}

- (void)detectAndDisplay:(cv::Mat&)img
{
    cv::Mat frame = img;
    std::vector<cv::Rect> faces;
    cv::Mat frame_gray;
    cv::Mat crop;
    cv::Mat res;
    cv::Mat gray;
    
    cvtColor(frame, frame_gray, cv::COLOR_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);
    
    // Detect faces
    
    _faceDetector.detectMultiScale(frame_gray, faces, 1.1, 3, 0 | cv::CASCADE_SCALE_IMAGE, cv::Size(60, 60));
    
    for(unsigned int i=0;i<faces.size();i++)
    {
        rectangle(frame,faces[i],cv::Scalar(255,0,0),1,8,0);
        cv::Mat face = frame(faces[i]);
        cvtColor(face,face,CV_BGR2GRAY);
        std::vector <cv::Rect> mouthi;
//        _mouthDetector.detectMultiScale(face, mouthi);
        
        cv::Rect ROI(0, face.cols / 3 * 2, face.rows, face.cols / 3);
        cv::Mat croppedImage = face(ROI);
        
//        UIImage *img = [UIImage UIImageFromCVMat:croppedImage];
        
        _mouthDetector.detectMultiScale(croppedImage, mouthi, 1.1, 2, 0, cv::Size(60, 60));

        for(unsigned int k=0;k<mouthi.size();k++)
        {
            cv::Point pt1(mouthi[k].x+faces[i].x , mouthi[k].y+faces[i].y + face.cols / 3 * 2);
            cv::Point pt2(pt1.x+mouthi[k].width, pt1.y+mouthi[k].height);
            rectangle(frame, pt1,pt2,cv::Scalar(0,255,0),1,8,0);
            
            if (mouthi[k].height >= face.rows / 5) {
                NSLog(@"Open");
            } else {
                NSLog(@"Close");
            }
            
            NSLog(@"%f", (float)mouthi[k].height / (float)face.rows);
            
        }
        
    }
    
//    imshow("Frame", frame);
}


@end
