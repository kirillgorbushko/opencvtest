//
//  openCVCaptureSession.h
//  VisualHeartRate
//
//  Created by Kirill Gorbushko on 29.02.16.
//  Copyright © 2016 - present thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
    ////openCV 3.x
    //#import <opencv2/videoio/cap_ios.h>

    //opencv 2.x
#import <opencv2/highgui/cap_ios.h>

@interface openCVCaptureSession : NSObject <CvVideoCameraDelegate>

- (instancetype _Nonnull)initWithCameraView:(UIView * _Nonnull)view scale:(CGFloat)scale;

- (void)startCapture;
- (void)stopCapture;

- (NSArray * _Nonnull)detectedFaces;
- (UIImage * _Nonnull)faceWithIndex:(NSInteger)idx;

@end
