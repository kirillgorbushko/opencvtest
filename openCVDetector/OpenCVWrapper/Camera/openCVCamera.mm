//
//  openCVCamera.m
//  openCVDetector
//
//  Created by Kirill Gorbushko on 29.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#include <opencv2/opencv.hpp>
    ////openCV 3.x
    //#import <opencv2/videoio/cap_ios.h>

    //opencv 2.x
#import <opencv2/highgui/cap_ios.h>

#import "openCVCamera.h"

#import "UIImage+openCV.h"
#import "VideoCamera.h"

using namespace cv;

@interface openCVCamera() <CvVideoCameraDelegate>

@property (strong, nonatomic) VideoCamera *videoCamera;

@end

@implementation openCVCamera

    //MARK: LifeCYcle
    
- (void)dealloc 
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

    //MARK: Public
    
- (void)setupCamera:(UIImageView *)parentView
{
    self.videoCamera = [[VideoCamera alloc] initWithParentView:parentView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset1280x720;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.delegate = self;
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)startCamera
{
    [self.videoCamera start];
}

#pragma mark - Notification

- (void)didRotate 
{
    [self.videoCamera adjustLayoutToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

#pragma mark - CvVideoCameraDelegate

- (void)processImage:(Mat&)image
{
    cv::Mat cvMat;
    cvtColor(image, cvMat, CV_BGR2RGB);
    UIImage *capturedImage = [UIImage imageWithCVMat:cvMat];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didGetImage:)]) {
        [self.delegate didGetImage:capturedImage];
    }
    if (self.didCaptureImage) {
        self.didCaptureImage(capturedImage);
    }
}

@end
