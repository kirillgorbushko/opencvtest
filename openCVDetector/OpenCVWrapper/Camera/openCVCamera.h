//
//  openCVCamera.h
//  openCVDetector
//
//  Created by Kirill Gorbushko on 29.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol openCVCameraDelegate <NSObject>

@optional
- (void)didGetImage:(UIImage * _Nonnull)image;

@end

@interface openCVCamera : NSObject

@property (weak, nonatomic, nullable) id <openCVCameraDelegate> delegate;

@property (copy, nonatomic, nullable) void (^didCaptureImage)(UIImage *_Nonnull);

- (void)setupCamera:(UIImageView * _Nonnull)parentView;
- (void)startCamera;

@end
