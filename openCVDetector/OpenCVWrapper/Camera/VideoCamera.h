//
//  VideoCamera.h
//  openCVDetector
//
//  Created by Kirill Gorbushko on 10.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <opencv2/opencv.hpp>

    ////openCV 3.x
//#import <opencv2/videoio/cap_ios.h>

 //opencv 2.x
#include <opencv2/highgui/cap_ios.h>

@protocol VideoCameraDelegate <CvVideoCameraDelegate>
@end

@interface VideoCamera : CvVideoCamera

- (void)updateOrientation;
- (void)layoutPreviewLayer;

@end
