//
//  openCVMSERCamera.m
//  openCVDetector
//
//  Created by Kirill Gorbushko on 11.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

#import "openCVMSERCamera.h"

#import <AVFoundation/AVFoundation.h>
#include <opencv2/opencv.hpp>
    ////openCV 3.x
    //#import <opencv2/videoio/cap_ios.h>

    //opencv 2.x
#import <opencv2/highgui/cap_ios.h>

#import "openCVCamera.h"

#import "UIImage+openCV.h"
#import "VideoCamera.h"

#import "MSERFeature.h"
#import "MSERManager.h"
#import "MLManager.h"
#import "FPS.h"

    //this two values are dependant on defaultAVCaptureSessionPreset
#define W (480)
#define H (640)

using namespace cv;

@interface openCVMSERCamera() <CvVideoCameraDelegate>

@property (strong, nonatomic) VideoCamera *videoCamera;

@end

@implementation openCVMSERCamera

    //MARK: LifeCYcle

- (void)dealloc 
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

    //MARK: Public

- (void)setupCamera:(UIImageView *)parentView
{
    self.videoCamera = [[VideoCamera alloc] initWithParentView:parentView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)startCamera
{
    [self.videoCamera start];
}

#pragma mark - Notification

- (void)didRotate 
{
    [self.videoCamera adjustLayoutToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

#pragma mark - CvVideoCameraDelegate

- (void)processImage:(Mat&)image
{        
    cv::Mat gray;
    cvtColor(image, gray, CV_BGRA2GRAY);
    
    std::vector<std::vector<cv::Point>> msers;
    [[MSERManager sharedInstance] detectRegions: gray intoVector: msers];
    if (msers.size() == 0) { return; };
    
    std::vector<cv::Point> *bestMser = nil;
    double bestPoint = 10.0;
    
    std::for_each(msers.begin(), msers.end(), [&] (std::vector<cv::Point> &mser)  {
        MSERFeature *feature = [[MSERManager sharedInstance] extractFeature: &mser];
        
        if(feature != nil) {
            if([[MLManager sharedInstance] isToptalLogo: feature] ) {
                double tmp = [[MLManager sharedInstance] distance: feature ];
                if ( bestPoint > tmp ) {
                    bestPoint = tmp;
                    bestMser = &mser;
                }
//                [UIImage drawMser: &mser intoImage: &image withColor: GREEN];
            }   else  {
//                [UIImage drawMser: &mser intoImage: &image withColor: RED];
            }
        } else  {
//            [UIImage drawMser: &mser intoImage: &image withColor: BLUE];
        }
    });
    
    if (bestMser) {
        NSLog(@"minDist: %f", bestPoint);
        
        cv::Rect bound = cv::boundingRect(*bestMser);
        cv::rectangle(image, bound, GREEN, 3);
        
    } else  {
        cv::rectangle(image, cv::Rect(0, 0, W, H), RED, 3);
    }
    
#if DEBUG    
    const char* str_fps = [[NSString stringWithFormat: @"MSER: %ld", msers.size()] cStringUsingEncoding: NSUTF8StringEncoding];
    cv::putText(image, str_fps, cv::Point(10, H - 10), CV_FONT_HERSHEY_PLAIN, 1.0, RED);
#endif
    
    [FPS draw: image]; 
}

@end

