//
//  OpenCVWrapper.m
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

#include <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"


#import "stitching.h"
#import "UIImage+Rotate.h"
#import "UIImage+openCV.h"

#import "openCVCamera.h"

#import "TemplateMatch.h"

@interface OpenCVWrapper() <TemplateMatchDelegate>

@property (strong, nonatomic) openCVCamera *cameraSession;
@property (strong, nonatomic) TemplateMatch *matcher;

@end

@implementation OpenCVWrapper

    //MARK: Match 
    
- (void)prepareMatcherWithTemplate:(UIImage * _Nonnull)source
{
    self.matcher = [[TemplateMatch alloc] initWithtemplateImage:source];
    self.matcher.delegate = self;
}
    
- (void)analyzeImage:(UIImage * _Nonnull)source
{
    [self.matcher analyzeImage:source];
}

#pragma mark - TemplateMatchDelegate

- (void)templateMatchDidReceiveTemplateImage:(UIImage *)templateImage 
{
    if (self.didReceiveTemplateImage) {
        self.didReceiveTemplateImage(templateImage);
    }
}

- (void)templateMatchDidReceiveAnalyzedImage:(UIImage *)analyzedImage
{
    if (self.didReceiveAnalyzedImage) {
        self.didReceiveAnalyzedImage(analyzedImage);
    }
}

- (void)templateMatchDidDetectObj:(BOOL)detect
{
    if (self.didDetectObj) {
        self.didDetectObj(detect);
    }
}

    //MARK: Stitch

- (UIImage * _Nonnull)stitchImages:( NSArray <UIImage *> * _Nonnull)sourceImages
{
    std::vector<cv::Mat> matImages;
    for (UIImage *sample in sourceImages) {
        UIImage* rotatedImage = [sample rotateToImageOrientation];
        cv::Mat matImage = [rotatedImage CVMat3];
        matImages.push_back(matImage);
    }
    cv::Mat stitchedMat = stitch(matImages);
    UIImage *stitchedImage =  [UIImage imageWithCVMat:stitchedMat];
    
    return stitchedImage;
}

    //MARK: Camera


- (void)startCameraSessionWith:(UIImageView * _Nonnull)parentView captureBlock:(void (^)(UIImage * _Nonnull))captureImageBlock
{
    self.cameraSession = [[openCVCamera alloc] init];
    [self.cameraSession setupCamera:parentView];
    [self.cameraSession startCamera];
    
    self.cameraSession.didCaptureImage = ^(UIImage *capturedImage) {
        if (captureImageBlock) {
            captureImageBlock(capturedImage);
        }
    };
}


@end
