//
//  SelectPhotoViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class SelectPhotoViewController : UIViewController {
    
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    fileprivate var mediaProvider:MediaProvider?
    
    fileprivate var selectedIndexPath:[IndexPath] = []
    
    var didCompletePhotoSelection:(([UIImage])->())?

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localizeUI()
        
        mediaProvider = MediaProvider(collectionView: collectionView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        selectedIndexPath = []
        mediaProvider?.reload()
        let set = IndexSet(integer: 0)
        collectionView.reloadSections(set)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let didCompletePhotoSelection = didCompletePhotoSelection {
            
            mediaProvider?.fetchPhotosFor(selectedIndexPaths: selectedIndexPath, offset: 1, completion: { (result) in
                didCompletePhotoSelection(result)
            })
        }
    }
    
    // MARK: - Private 
    
    fileprivate func localizeUI() {
        title = "Select photos"
    } 
    
    fileprivate func setupUI() {
        let layout = AJFCollectionViewWaterfallLayout()
        layout.stretchingType = .stretchLastCell
        collectionView.collectionViewLayout = layout
    }

}

extension SelectPhotoViewController : UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            let sideSize = (collectionView.frame.width - 20) / 3  
            return CGSize(width: sideSize, height: sideSize)
        } else {
            return mediaProvider?.calculateTargetSizeForItemAt(index: indexPath.row - 1) ?? CGSize.zero
        }
    }
}

extension SelectPhotoViewController : UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            collectionView.deselectItem(at: indexPath, animated: true)
        } else {
//            mediaProvider?.imageAtIndexPath(index: indexPath.row - 1, handler: { (requestedImage) in
//                
//            })
            if let cell:PreviewCollectionViewCell = self.collectionView.cellForItem(at: indexPath) as? PreviewCollectionViewCell {
            
                if selectedIndexPath.contains(indexPath) {
                    if let index = selectedIndexPath.index(of: indexPath) {
                        selectedIndexPath.remove(at: index)
                        cell.setItemSelected(selected: false)
                    }
                    
                } else {
                    selectedIndexPath.append(indexPath)
                    cell.setItemSelected(selected: true)
                }
            }
        }
    }
}

extension SelectPhotoViewController : UICollectionViewDataSource {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var photosCount:Int = 0
        if let provider = mediaProvider {
           photosCount = provider.itemsCount()
        }
        return 1 + photosCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell:CameraPreviewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: CameraPreviewCollectionViewCell.identifier(), for: indexPath) as! CameraPreviewCollectionViewCell
            return cell
        } else {
            let cell:PreviewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: PreviewCollectionViewCell.identifier(), for: indexPath) as! PreviewCollectionViewCell
            cell.previewType = .selectablePhoto
            
            if selectedIndexPath.contains(indexPath) {
                cell.setItemSelected(selected: true)
            }

            let size:CGSize = CGSize(width: collectionView.frame.height / 5, height: collectionView.frame.width / 3)
            mediaProvider?.thumbnailForIndexPath(index: indexPath.row - 1, targetSize: size, handler: { (image) in
                cell.previewImageView.image = image
            })
            return cell
        }
    }
}

extension SelectPhotoViewController : AJFCollectionViewWaterfallLayoutDelegate {
    // MARK: - AJFCollectionViewWaterfallLayoutDelegate
    
    func collectionView(_ collectionView: UICollectionView!, numberOfColumnsInSection section: Int) -> Int {
        return 4
    }
}

extension SelectPhotoViewController : UIScrollViewDelegate {
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        mediaProvider?.updatePrecheatedRect(controller: self)
    }
}
