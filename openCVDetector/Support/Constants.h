//
//  Constants.h
//  openCVDetector
//
//  Created by Kirill Gorbushko on 10.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#pragma mark - Defines

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_BOUNDS  [UIScreen mainScreen].bounds
#define APPLICATION [UIApplication sharedApplication]
#define USER_DEFAULTS [NSUserDefaults standartUserDefaults]

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define KEYBOARD_HEIGHT_IPHONE_4_OR_LESS 253
#define KEYBOARD_HEIGHT_IPHONE_5 253
#define KEYBOARD_HEIGHT_IPHONE_6 258
#define KEYBOARD_HEIGHT_IPHONE_6_PLUS 271

#pragma mark - DLog

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)


#endif /* Constants_h */
