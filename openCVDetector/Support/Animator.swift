//
//  Animator.swift
//  
//
//  Created by Kirill Gorbushko on 30.07.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class Animator {
    
    fileprivate enum Defaults {
        static let AnimationDuration:TimeInterval = 0.3
    }
    
    class func scaleAnimation() -> CAKeyframeAnimation {
        
        let scaleAnimation:CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
        scaleAnimation.values = [
            NSValue(caTransform3D: CATransform3DIdentity),
            NSValue(caTransform3D: CATransform3DMakeScale(1.3, 1.3, 1.3)),
            NSValue(caTransform3D: CATransform3DIdentity)
        ]
        scaleAnimation.duration = Defaults.AnimationDuration * 1.5
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        return scaleAnimation
    }
    
    class func scaleOutAnimation(_ view:UIView) {
        let scaleAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform")
        scaleAnimation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnimation.toValue =  NSValue(caTransform3D: CATransform3DMakeScale(0.8, 0.8, 0.8))
        scaleAnimation.duration = Defaults.AnimationDuration
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.layer.add(scaleAnimation, forKey: nil)
        view.layer.transform = CATransform3DMakeScale(0.8, 0.8, 0.8)
    }
    
    class func fadeAnimation(_ fromValue:CGFloat, toValue:CGFloat, delegate:AnyObject? = nil) -> CABasicAnimation {
        let fadeAnim:CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        fadeAnim.fromValue = fromValue
        fadeAnim.toValue = toValue
        fadeAnim.duration = Defaults.AnimationDuration
        if (delegate != nil) {
            fadeAnim.isRemovedOnCompletion = false
            fadeAnim.delegate = delegate as! CAAnimationDelegate?
        } else {
            fadeAnim.isRemovedOnCompletion = true
        }
        fadeAnim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        return fadeAnim
    }
    
    class func shakeAnimation(_ view:UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.06
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.isRemovedOnCompletion = true
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 7, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 7, y: view.center.y))
        view.layer.add(animation, forKey: nil)
    }
    
    class func applyRotationAnimation(_ clocwise:Bool, view:UIView, updateViewHandler:(()->Void)? = nil, completion:(()->Void)? = nil) {
        
        var rotationAndPerspectiveTransform = view.layer.transform
        if clocwise == true {
            rotationAndPerspectiveTransform.m34 = 1.0 / 500.0
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, CGFloat(M_PI), 0, 1, 0)
        } else {
            rotationAndPerspectiveTransform.m34 = 1.0 / -500.0
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -CGFloat(M_PI), 0, 1, 0)
        }
        
        let transformAnimation = CABasicAnimation(keyPath: "transform")
        transformAnimation.fromValue =  NSValue(caTransform3D:view.layer.transform)
        transformAnimation.toValue = NSValue(caTransform3D:rotationAndPerspectiveTransform)
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        var startScale = CATransform3DScale(view.layer.transform, 1, 0, 0)
        var midScale = CATransform3DScale(view.layer.transform, 0.8, 0, 0)
        var endScale = CATransform3DScale(view.layer.transform, 1, 0, 0)
        
        if clocwise == false {
            startScale = CATransform3DScale(view.layer.transform, 1, -1, 1);
            midScale = CATransform3DScale(view.layer.transform, 0.8, -0.8, 0.8);
            endScale = CATransform3DScale(view.layer.transform, 1, -1, 1);
        }
        
        scaleAnimation.values = [
            NSValue(caTransform3D:startScale),
            NSValue(caTransform3D:midScale),
            NSValue(caTransform3D:endScale),
        ]
        
//        scaleAnimation.keyTimes = [0.0, 0.5, 0.9]
        scaleAnimation.timingFunctions = [
            CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut),
            CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        ]
        
        scaleAnimation.fillMode = kCAFillModeForwards
        scaleAnimation.isRemovedOnCompletion = false
        
        let animationGroup = CAAnimationGroup()
        animationGroup.animations = [
            transformAnimation,
            scaleAnimation
        ]
        
        let animationDuration = 0.4
        animationGroup.duration = animationDuration
        
        var delayTime = DispatchTime.now() + Double(Int64(animationDuration / 2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            if let updateViewHandler = updateViewHandler {
                updateViewHandler()
            }
        })
        
        delayTime = DispatchTime.now() + Double(Int64(animationDuration  * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            if let completion = completion {
                completion()
            }
        })

        
        view.layer.add(animationGroup, forKey: nil)
        view.layer.transform = rotationAndPerspectiveTransform
    }
    
    static func swipeTransitionToLeftSide(_ leftSide:Bool) -> CATransition {
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = leftSide ? kCATransitionFromRight : kCATransitionFromLeft
        transition.duration = Defaults.AnimationDuration
        return transition
    }
    
    static func cornerRadius(_ from: CGFloat, to: CGFloat, duration: CFTimeInterval = 0.3) -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        
        return animation
    }
}
