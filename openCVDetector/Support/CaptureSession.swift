//
//  CaptureSession.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 28.12.16.
//  Copyright © 2016 - present SigmaSoftware. All rights reserved.
//

import UIKit
import AVFoundation

final class CaptureSession {
    
    fileprivate var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    fileprivate var captureSession: AVCaptureSession!
    fileprivate var previewViewLayer: UIView!
    
    init(_ previewView: UIView!) {
        
        previewViewLayer = previewView
        prepareSession()
    }
    
    // MARK: - Public
    
    class func checkPermissionForCamera(completion:((_ granted:Bool)->Void)? = nil) {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { (granted) in
            DispatchQueue.main.async {
                completion?(granted)
            }
        }
    }
    
    class func hasBackCamera() -> Bool {
        var hasBackCamera = false
        guard let devices:[AVCaptureDevice] = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as? [AVCaptureDevice] else { return false }
        for device in devices {
            if device.position == .back {
                hasBackCamera = true
                break
            }
        }
        return hasBackCamera
    }

    func relayoutPreview() {
        videoPreviewLayer.frame = previewViewLayer.bounds
    }
    func start() {
        captureSession.startRunning()
    }
    
    func stop() {
        captureSession.stopRunning()
    }
    
    // MARK: - Private
    
    // MARK: - Preparation
    private func prepareSession() {
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice) as AVCaptureDeviceInput
            captureSession = AVCaptureSession()
            captureSession.addInput(input)
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer.frame = previewViewLayer.bounds
            previewViewLayer.layer.addSublayer(videoPreviewLayer)
        } catch { /*error */ }
    }

    
}
