//
//  mserViewController.swift
//  openCVDetector
//
//  Created by Kirill Gorbushko on 11.01.17.
//  Copyright © 2017 - present SigmaSoftware. All rights reserved.
//

import UIKit

final class mserViewController : UIViewController {
    
    @IBOutlet fileprivate weak var previewImageView: UIImageView!
    
    fileprivate var camera:openCVMSERCamera = openCVMSERCamera()
    
    fileprivate var currentFrame:UIImage?

    // MARK: - LifeCycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        camera.setupCamera(previewImageView)
        camera.start()
        
    }

}
