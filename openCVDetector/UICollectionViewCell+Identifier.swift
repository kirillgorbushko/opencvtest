//
//  UICollectionViewCell+Identifier.swift
//  SuavooClient
//
//  Created by Kirill Gorbushko on 08.10.16.
//  Copyright © 2016 Suavoo. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    // MARK: - UICollectionViewCell+Identifier
    
    class func identifier() -> String {
        return String(describing: self)
    }
    
    class func suavooNib() -> UINib {
        return UINib.init(nibName: self.identifier(), bundle:nil)
    }
}
